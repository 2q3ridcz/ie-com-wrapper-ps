﻿using module ..\Class\IEComWrapper.psm1

<#
.Synopsis
   Gets an existing internet explorer com object.
.DESCRIPTION
   Reference: https://qiita.com/flasksrw/items/a1ff5fbbc3b660e01d96.
.EXAMPLE
   Get-IEComWrapper -LocationName "*MSN*"
#>
function Get-IEComWrapper {
    [CmdletBinding()]
    [OutputType([IEComWrapper])]
    param (
        [Parameter(Mandatory=$False)]
        [String]
        $HWND = "*"
        ,
        [Parameter(Mandatory=$False)]
        [String]
        $LocationName = "*"
        ,
        [Parameter(Mandatory=$False)]
        [String]
        $LocationURL = "*"
    )
    
    begin {
        $shell = New-Object -ComObject Shell.Application
    }
    
    process {
        $LocationURL = $LocationURL.Replace("\", "/")

        $shell.Windows() |
        Where-Object {$_.Name -eq "Internet Explorer"} |
        Where-Object {$_.HWND -like $HWND} |
        Where-Object {$_.LocationName -like $LocationName} |
        Where-Object {$_.LocationURL -like $LocationURL} |
        ForEach-Object { [IEComWrapper]::new($_) }
    }
    
    end {
    }
}
