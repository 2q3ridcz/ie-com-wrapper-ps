﻿# using assembly System.Drawing
using assembly Microsoft.VisualBasic
using assembly System.Windows.Forms

$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$FunctionFolderPath = "$here\Function" | Resolve-Path

# .("$here\ie.ps1")

Get-ChildItem -Path $FunctionFolderPath |
Where-Object { $_.Extension -Eq ".psm1" } |
ForEach-Object {
    Import-Module -Name $_.FullName -Force
    Export-ModuleMember -Function $_.BaseName
}
