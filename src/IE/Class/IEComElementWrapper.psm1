﻿### Receives an element via the constructor.
### What is an element? Check: https://developer.mozilla.org/ja/docs/Web/API/Element
class IEComElementWrapper {
    ### Property name is 'Node' for compatiblity with IEComNodeWrapper.
    [__ComObject] $Node

    IEComElementWrapper([__ComObject] $Node) {
        try {
            If ( $Null -Ne $Node.documentElement ) {
                ### If $Element is a document (not an element), 
                ### convert it to an element.
                $this.Node = $Node.documentElement
            } ElseIf ( $Null -Eq $Node.outerHTML ) {
                ### If $Element not an element, try getting html element.
                $this.Node = $Node.querySelector("html")
            } Else {
                $this.Node = $Node
            }
        } catch {
            $this.Node = $Node
        }
    }

    [IEComElementWrapper] GetParentNode() {
        return [IEComElementWrapper]::new($this.Node.parentNode)
    }

    [Object] InvokeComMethod($MethodName, [array]$ArgumentList) {
        return [__ComObject].InvokeMember(
            $MethodName, [System.Reflection.BindingFlags]::InvokeMethod, $null, $this.Node, $ArgumentList
        )
    }

    [IEComElementWrapper[]] InvokeComMethodAndGetIEComElementWrapper($MethodName, [array]$ArgumentList) {
        return (
            $this.InvokeComMethod($MethodName, $ArgumentList) |
            ForEach-Object { [IEComElementWrapper]::new($_) }
        )
    }
    
    [IEComElementWrapper[]] QuerySelectorAll([string]$Selector) {
        $nodes = [System.__ComObject].InvokeMember("querySelectorAll", [System.Reflection.BindingFlags]::InvokeMethod, $null, $this.Node, $Selector)
        $result = New-Object System.Collections.Generic.List[IEComElementWrapper]
        for ($i = 0; $i -lt $nodes.Length; $i++) {
            $result.Add([IEComElementWrapper]::new(
                [System.__ComObject].InvokeMember("item", [System.Reflection.BindingFlags]::InvokeMethod, $null, $nodes, $i)
            ))
        }
        return $result
    }

    [IEComElementWrapper] QuerySelector([string]$Selector) {
        return ($this.InvokeComMethodAndGetIEComElementWrapper("querySelector", $Selector))[0]
    }
    
    [IEComElementWrapper] GetElementById([string]$Id) {
        # return ($this.InvokeComMethodAndGetIEComElementWrapper("getElementById", $Id))[0]
        return $this.QuerySelector("#" + $Id)
    }

    [IEComElementWrapper[]] GetElementsByName([string]$Name){
        # return $this.InvokeComMethodAndGetIEComElementWrapper("getElementsByName", $Name)
        return $this.QuerySelectorAll('[name="' + $Name + '"]')
    }

    [IEComElementWrapper[]] GetElementsByTagName([string]$TagName){
        return $this.InvokeComMethodAndGetIEComElementWrapper("getElementsByTagName", $TagName)
    }

    [IEComElementWrapper[]] GetElementsByClassName([string]$ClassName){
        return $this.InvokeComMethodAndGetIEComElementWrapper("getElementsByClassName", $ClassName)
    }

    [bool] ScrollHorizontal() {
        $Before = $this.Node.scrollLeft
        $this.Node.scrollLeft = $this.Node.scrollLeft + $this.Node.clientWidth
        return -Not ($Before -Eq $this.Node.scrollLeft)
    }

    [bool] ScrollVertical() {
        $Before = $this.Node.scrollTop
        $this.Node.scrollTop = $this.Node.scrollTop + $this.Node.clientHeight
        return -Not ($Before -Eq $this.Node.scrollTop)
    }

    [void] SaveHtmlFile([string]$Path, [string]$Encoding) {
        $this.Node.outerHTML |
        Out-File -FilePath $Path -Encoding $Encoding
    }
}