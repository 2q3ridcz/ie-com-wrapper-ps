﻿using module .\IEComNodeWrapper.psm1
using module .\IEComElementWrapper.psm1

class IEComWrapper {
    [__ComObject] $IE

    IEComWrapper() {
        $this.IE = New-Object -ComObject "InternetExplorer.Application"
    }

    IEComWrapper($IE) {
        $this.IE = $IE
    }

    [IEComNodeWrapper] GetDocumentWrapper() {
        return [IEComNodeWrapper]::new($this.IE.Document)
    }

    [IEComElementWrapper] GetDocumentElementWrapper() {
        return [IEComElementWrapper]::new($this.IE.Document)
    }

    [void] Focus() {
        Add-Type -AssemblyName Microsoft.VisualBasic
        
        $window_process = Get-Process -Name "iexplore" |
        Where-Object {$_.MainWindowHandle -eq $this.IE.HWND}
        
        [Microsoft.VisualBasic.Interaction]::AppActivate($window_process.ID) | Out-Null
    }

    [void] Quit() {
        try {
            $this.IE.Stop()
            $this.IE.Quit()
        } finally {
            $this.IE = $Null
            [GC]::Collect()
        }
    }

    # IEのページ読み込みが完了するまで待機する
    [void] Wait() {
        # ビジーでない状態が$OkSeconds秒継続したら読み込み終わったと判断する
        $OkSeconds=0.5
        $limit =( get-date ).AddSeconds( $OkSeconds )
        do{
            if (($this.IE.Busy) -or ( $this.IE.ReadyState -ne 4 )){
                $limit =( get-date ).AddSeconds( $OkSeconds )
            }
            Start-Sleep -m 100
        }while( ( get-date ).CompareTo( $limit ) -lt 0 )
    }

    #　要素の出現を待つ（要素はCSSセレクタで指定）
    [void] WaitForElement([string]$Selector) {
        $this.Wait()
        while( $true ){
            try{
                if ( $e=$this.GetDocumentElementWrapper().QuerySelector($selector) ){
                    break
                }
            }catch{[void]"err"}
            [void]"Loop"
        }
    }

    [bool] IsVisible() {
        return $this.IE.Visible
    }

    [void] SetVisible([bool] $Visible) {
        $this.IE.Visible = $Visible
    }

    #URLを開く
    [void] Navigate([String] $Url) {
        $hwnd = $this.IE.HWND
        $this.IE.Navigate($Url)

        # https://qiita.com/flasksrw/items/a1ff5fbbc3b660e01d96
        # > IEの保護モードがオフの時は、-ComObjectにInternetExplorer.ApplicationMediumを
        # > 指定するか、管理者モードで起動しないと、URLを開いた後にIEが操作できなくなる。
        # > しかし、保護モードかどうかを知るのが難しく、管理者モードで起動できるとも限らない。
        # > このループはそれを解消するためのもので、$shell.Windows()からIEを取得しなおすと保護モードが
        # > オフでもIEが操作できる。ループにしたのは、$ie.Documentが取得できない場合があるため。
        $shell = New-Object -ComObject Shell.Application
        while ($Null -Eq $this.IE.Document) {
            $this.IE = $shell.Windows() | Where-Object {$_.HWND -eq $hwnd}
        }
    }

    # [void] SaveHtmlFile([string]$Path, [string]$Encoding) {
    #     $this.GetDocumentWrapper().QuerySelectorAll("html") |
    #     ForEach-Object { $_.Node.outerHTML } |
    #     Out-File -FilePath $Path -Encoding $Encoding
    # }

    [void] SaveScreenShot([string]$Path) {
        If (-Not ($Path | Split-Path -IsAbsolute)) {
            $Path = Get-Location | Join-Path -ChildPath $Path
        }

        $this.Focus()
        ### 備忘：CopyFromScreen方式から Alt+PrintScreen方式へ変更

        ### CopyFromScreen方式
        ### 出力画像が IE画面ぴったりにならず、少し外側の余計な部分まで保存される。
        # Add-Type -AssemblyName System.Drawing
        # $bitmap = new-object -TypeName System.Drawing.Bitmap -ArgumentList $this.IE.Width, $this.IE.Height
        # $graphics = [System.Drawing.Graphics]::FromImage($bitmap)
        # $graphics.CopyFromScreen($this.IE.Left, $this.IE.Top, 0, 0, $bitmap.Size)
        # $bitmap.Save($Path)

        ### Alt+PrintScreen方式
        ### 出力画像が IE画面ぴったりになる。
        ### 利用者のコピペ操作と干渉するのが嫌だが、やむ無し…
        Add-Type -AssemblyName System.Windows.Forms
        [System.Windows.Forms.Sendkeys]::SendWait("%{PrtSc}")
        Start-Sleep -Milliseconds 250
        $bitmap = [System.Windows.Forms.Clipboard]::GetImage()
        $bitmap.Save($Path)
        [System.Windows.Forms.Clipboard]::Clear()
    }
}