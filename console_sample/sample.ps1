$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$ProjectFolderPath = "$here\..\" | Resolve-Path

$PackageName = "IE"
$PackagePath = $ProjectFolderPath | Join-Path -ChildPath "src\$PackageName\$PackageName.psm1"
Import-Module -Name $PackagePath -Force



$IE = New-IEComWrapper
$IE.SetVisible($True)
$IE.Navigate("about:blank")
$IE.Wait()

$Doc = $IE.GetDocumentElementWrapper()
$body = ($Doc.GetElementsByTagName("body"))[0]

$body.Node.innerHTML = @"
<div id="number" style="height: 300px; width: 300px; overflow: auto;">
<br>
<p>0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz
<p>0</p><p>1</p><p>2</p><p>3</p><p>4</p><p>5</p><p>6</p><p>7</p><p>8</p><p>9</p>
<p>10</p><p>11</p><p>12</p><p>13</p><p>14</p><p>15</p><p>16</p><p>17</p><p>18</p><p>19</p>
<p>20</p><p>21</p><p>22</p><p>23</p><p>24</p><p>25</p><p>26</p><p>27</p><p>28</p><p>29</p>
</div>
"@

$Div = $Body.QuerySelector("#number")
Write-Host -Object ('$Div.Node.scrollTop: ' + $Div.Node.scrollTop)
Write-Host -Object ('$Div.Node.scrollHeight: ' + $Div.Node.scrollHeight)
Write-Host -Object ('$Div.Node.clientHeight: ' + $Div.Node.clientHeight)

$IE.Focus()
$LoopCount = 0
While( $Div.Node.scrollTop + $Div.Node.clientHeight -Lt $Div.Node.scrollHeight ) {
	Start-Sleep -Milliseconds 500
	$Div.Node.scrollTop = $Div.Node.scrollTop + $Div.Node.clientHeight
	$LoopCount += 1
}
Write-Host -Object ('VerticalScrollCount: ' + $LoopCount)

$IE.Focus()
$LoopCount = 0
While( $Div.Node.scrollLeft + $Div.Node.clientWidth -Lt $Div.Node.scrollWidth ) {
	Start-Sleep -Milliseconds 500
	$Div.Node.scrollLeft = $Div.Node.scrollLeft + $Div.Node.clientWidth
	$LoopCount += 1
}
Write-Host -Object ('HorizontalScrollCount: ' + $LoopCount)


Read-Host ("Fin!") | Out-Null

$IE.Quit()

