﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\..\src\IE\ie.ps1")

Describe "ie.ps1" {
    Context "Senario Tests" {
        It "Can open google result page" {
            # "(1)Googleの検索結果一覧を取得してみる"
            $ie =OpenUrl "https://www.google.co.jp/search?q=test" $True
            WaitIE $ie
            WaitForElement $ie "input.noHIxc"
            # "LOADED"
            $searchBox = querySelectorAll $ie.Document "input.noHIxc"
            $searchBox | % { $_.value } | Should Be "test"
            $ie.stop()
            $ie.quit()
        }

        It "Can run google search" {
            # "(2)別のキーワードで検索してみる"
            $ie =OpenUrl "https://www.google.co.jp/search?q=test" $True
            WaitIE $ie
            WaitForElement $ie "input.noHIxc"
            # "LOADED"
            $searchBox = querySelectorAll $ie.Document "input.noHIxc"
            $searchBox.Value = "time japan"
            $btn =  querySelector $ie.Document "#qdClwb"
            $btn.click()
            WaitIE $ie
            $resultElm = querySelectorAll $ie.Document "title"
            $resultElm | % { $_.innerText } | Should BeLike "time japan - Google*"
            $ie.stop()
            $ie.quit()
        }

        It "Can handle dom" {
            #dom操作を試みる
            $ie =OpenUrl "about:blank"
            WaitIE $ie
            $doc=$ie.document
            $body=getElementsByTagName $doc "body"
            #HTMLを直接書き込む
            $body.innerHTML=@"
<div>
<input type="radio" name="rd" value="1">a
<input type="radio" name="rd" value="2">b
<input type="radio" name="rd" value="3">c
</div>
"@
            #Dom操作でラジオボタンを追加
            $radio_div =(getElementsByName $doc "rd")[0].parentNode
            $radio_d =invokeComMethod $doc "createElement" "input"
            invokeComMethod $radio_d "setAttribute" "type" "radio"|out-null
            invokeComMethod $radio_d "setAttribute" "name" "rd"|out-null
            invokeComMethod $radio_div "appendChild" $radio_d|out-null
            $radio_txt_d=invokeComMethod $doc "createTextNode" "d"
            invokeComMethod $radio_div "appendChild" $radio_txt_d|out-null
            $ie.Visible = $true
            <# 64bit環境では scriptcontrolの呼び出しで COMExceptionが発生するため使用不可。
            IeActivate $ie
            sleep 2
            #>
            #Dom操作でラジオボタンをチェック
            $radios =getElementsByName $doc "rd"
            $radio=$radios[1]
            invokeComMethod $radio "setAttribute" "checked"|out-null
            sleep 4
            <# 64bit環境では scriptcontrolの呼び出しで COMExceptionが発生するため使用不可。
            $ie.Visible = $false
            #Windowを消してアラート表示
            windowMethod $ie "alert" "`"aaa`""
            #Windowプロパティ取得
            windowProperty $ie "location.Href"
            #Windowプロパティ代入
            windowProperty $ie "location.Href" "https://www.infoseek.co.jp/"
            WaitIE $ie
            $ie.Visible = $true
            IeActivate $ie
            #>
            $SelectedElm = querySelector $ie.Document "input[name='rd'][checked='']"
            $SelectedElm | % { $_.value } | Should Be "2"
            $ie.stop()
            $ie.quit()
        }
    }
}
