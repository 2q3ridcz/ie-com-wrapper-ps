﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\Import-TargetPackage.ps1")

Describe "IEComElementWrapper" {
    BeforeEach {
        $IE = New-IEComWrapper
        $IE.SetVisible($True)
        $IE.Navigate("about:blank")
        $IE.Wait()
        $Doc = $IE.GetDocumentElementWrapper()
    }

    AfterEach {
        If ($Null -Eq $IE) {
            $IE = Get-IEComWrapper -LocationName "about:blank"
        }
        If ($IE) {
            $IE.Quit()
        }
    }

    Context "SaveHtmlFile" {
        It "Can save to html file" {
            $testPath = "TestDrive:\test.txt"

            $Doc.SaveHtmlFile($testPath, "UTF8")
            Get-Content -Path $testPath | Should Be "<html><head></head><body></body></html>"
        }
    }

    Context "QuerySelector" {
        It "Returns a single element" {
            $Body = $Doc.QuerySelector("body")
            $Body.Node.innerHTML = @"
<ul>
<li>a</li>
<li>b</li>
</ul>
<ul>
<li>c</li>
<li>d</li>
<li>e</li>
</ul>
"@
            $Lis = $Doc.QuerySelector("li")
            $Lis.Length | Should Be 1
            $Lis.Node.innerHTML | Should Be "a"
        }
    }

    Context "QuerySelectorAll" {
        It "Returns an array with single element" {
            $Body = $Doc.QuerySelectorAll("body")
            $Body.Length | Should Be 1
            $Body.Node.outerHTML | Should Be "<body></body>"
        }

        It "Returns an array with multiple elements" {
            $Body = ($Doc.QuerySelectorAll("body"))[0]
            $Body.Node.innerHTML = @"
<ul>
<li>a</li>
<li>b</li>
</ul>
<ul>
<li>c</li>
<li>d</li>
<li>e</li>
</ul>
"@
            $Lis = $Doc.QuerySelectorAll("li")
            $Lis.Length | Should Be 5
            $Lis[3].Node.innerHTML | Should Be "d"
        }
    }

    Context "GetElementById" {
        It "Returns a single element" {
            $Body = $Doc.GetElementsByTagName("body")[0]
            $Body.Node.innerHTML = @"
<ul>
<li>a</li>
<li id="target">b</li>
</ul>
<ul>
<li>c</li>
<li>d</li>
<li>e</li>
</ul>
"@
            $Lis = $Doc.GetElementById("target")
            $Lis.Length | Should Be 1
            $Lis.Node.innerHTML | Should Be "b"
        }
    }

    Context "GetElementsByName" {
        It "Returns an array with multiple elements" {
            $Body = ($Doc.GetElementsByTagName("body"))[0]
            $Body.Node.innerHTML = @"
<ul>
<li>a</li>
<li>b</li>
</ul>
<ul>
<li>c</li>
<li>d</li>
<li name="hello">e</li>
</ul>
<a name="hello">link</a>
"@
            $Lis = $Doc.GetElementsByName("hello")
            $Lis.Length | Should Be 2
            $Lis[0].Node.innerHTML | Should Be "e"
            $Lis[1].Node.innerHTML | Should Be "link"
        }
    }

    Context "GetElementsByTagName" {
        It "Returns an array with single element" {
            $Body = $Doc.GetElementsByTagName("body")
            $Body.Length | Should Be 1
            $Body.Node.outerHTML | Should Be "<body></body>"
        }

        It "Returns an array with multiple elements" {
            $Body = ($Doc.GetElementsByTagName("body"))[0]
            $Body.Node.innerHTML = @"
<ul>
<li>a</li>
<li>b</li>
</ul>
<ul>
<li>c</li>
<li>d</li>
<li>e</li>
</ul>
"@
            $Lis = $Doc.GetElementsByTagName("li")
            $Lis.Length | Should Be 5
            $Lis[3].Node.innerHTML | Should Be "d"
        }
    }

    Context "GetElementsByClassName" {
        It "Returns an array with multiple elements" {
            $Body = ($Doc.GetElementsByTagName("body"))[0]
            $Body.Node.innerHTML = @"
<ul>
<li class="class1">a</li>
<li>b</li>
</ul>
<ul>
<li>c</li>
<li class="class1">d</li>
<li>e</li>
</ul>
"@
            $Lis = $Doc.GetElementsByClassName("class1")
            $Lis.Length | Should Be 2
            $Lis[0].Node.innerHTML | Should Be "a"
            $Lis[1].Node.innerHTML | Should Be "d"
        }
    }

    Context "ScrollHorizontal" {
        It "Returns false when it cannot scroll" {
            $body = ($Doc.GetElementsByTagName("body"))[0]

            $body.Node.innerHTML = @"
<div id="number" style="height: 100px; width: 100px; overflow: auto;"><br></div>
"@

            $Div = $Body.QuerySelector("#number")

            $ExpectScrollCount = [Math]::Ceiling($Div.Node.scrollWidth / $Div.Node.clientWidth)

            ### Check if testing requirement is fulfilled.
            $Div.Node.scrollLeft | Should Be 0
            $ExpectScrollCount | Should Be 1

            $Div.ScrollHorizontal() | Should Be $False
        }

        It "Returns true until it scrolls to end" {
            $body = ($Doc.GetElementsByTagName("body"))[0]

            $body.Node.innerHTML = @"
<div id="number" style="height: 100px; width: 100px; overflow: auto;">
<br>
<p>0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz
<p>0</p><p>1</p><p>2</p><p>3</p><p>4</p><p>5</p><p>6</p><p>7</p><p>8</p><p>9</p>
<p>10</p><p>11</p><p>12</p><p>13</p><p>14</p><p>15</p><p>16</p><p>17</p><p>18</p><p>19</p>
<p>20</p><p>21</p><p>22</p><p>23</p><p>24</p><p>25</p><p>26</p><p>27</p><p>28</p><p>29</p>
</div>
"@

            $Div = $Body.QuerySelector("#number")

            $ExpectScrollCount = [Math]::Ceiling($Div.Node.scrollWidth / $Div.Node.clientWidth)

            ### Check if testing requirement is fulfilled.
            $Div.Node.scrollLeft | Should Be 0
            3 -Le $ExpectScrollCount | Should Be $True

            $ScrollCount = 0
            Do {
                $Res = $Div.ScrollHorizontal()
                $ScrollCount += 1
            } While( $Res -Eq $True )
            $ScrollCount | Should Be $ExpectScrollCount
        }
    }

    Context "ScrollVertical" {
        It "Returns false when it cannot scroll" {
            $body = ($Doc.GetElementsByTagName("body"))[0]

            $body.Node.innerHTML = @"
<div id="number" style="height: 100px; width: 100px; overflow: auto;"><br></div>
"@

            $Div = $Body.QuerySelector("#number")

            $ExpectScrollCount = [Math]::Ceiling($Div.Node.scrollHeight / $Div.Node.clientHeight)

            ### Check if testing requirement is fulfilled.
            $Div.Node.scrollTop | Should Be 0
            $ExpectScrollCount | Should Be 1

            $Div.ScrollVertical() | Should Be $False
        }

        It "Returns true until it scrolls to end" {
            $body = ($Doc.GetElementsByTagName("body"))[0]

            $body.Node.innerHTML = @"
<div id="number" style="height: 100px; width: 100px; overflow: auto;">
<br>
<p>0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz
<p>0</p><p>1</p><p>2</p><p>3</p><p>4</p><p>5</p><p>6</p><p>7</p><p>8</p><p>9</p>
<p>10</p><p>11</p><p>12</p><p>13</p><p>14</p><p>15</p><p>16</p><p>17</p><p>18</p><p>19</p>
<p>20</p><p>21</p><p>22</p><p>23</p><p>24</p><p>25</p><p>26</p><p>27</p><p>28</p><p>29</p>
</div>
"@

            $Div = $Body.QuerySelector("#number")

            $ExpectScrollCount = [Math]::Ceiling($Div.Node.scrollHeight / $Div.Node.clientHeight)

            ### Check if testing requirement is fulfilled.
            $Div.Node.scrollTop | Should Be 0
            3 -Le $ExpectScrollCount | Should Be $True

            $ScrollCount = 0
            Do {
                $Res = $Div.ScrollVertical()
                $ScrollCount += 1
            } While( $Res -Eq $True )
            $ScrollCount | Should Be $ExpectScrollCount
        }
    }

    Context "GetParentNode" {
        It "Returns a parent node" {
            $Parent = $Doc.QuerySelector("body").GetParentNode()
            $Parent.Node.outerHTML | Should Be "<html><head></head><body></body></html>"
        }
    }
}

Describe "IEComElementWrapper.Node" {
    BeforeEach {
        $IE = New-IEComWrapper
        $IE.SetVisible($True)
        $IE.Navigate("about:blank")
        $IE.Wait()
        $Doc = $IE.GetDocumentElementWrapper()
        $Node = $Doc.Node
    }

    AfterEach {
        $IE.Quit()
    }

    Context "QuerySelector" {
        It "Returns a single element" {
            $Body = $Node.QuerySelector("body")
            $Body.innerHTML = @"
<ul>
<li>a</li>
<li>b</li>
</ul>
<ul>
<li>c</li>
<li>d</li>
<li>e</li>
</ul>
"@
            $Lis = $Node.QuerySelector("li")
            $Lis.innerHTML | Should Be "a"
        }
    }

    <#
    ### $Doc.querySelectorAll("body")でエラーが発生する。
    ### 「COMException: RPC サーバーを利用できません。 (HRESULT からの例外:0x800706BA) 」
    ### なぜか Should Throwで捕まえられない…
    Context "QuerySelectorAll" {
        It "Throws" {
            # $Node.querySelectorAll("body") | ForEach-Object { $_.outerHTML } | Should Be "<body></body>"
            { $Node.querySelectorAll("body") } | Should Throw
        }
    }
    #>

    Context "GetElementsByTagName" {
        It "Returns an array with single element" {
            $Body = $Node.GetElementsByTagName("body")
            $Body.Length | Should Be 1
            $Body[0] | ForEach-Object { $_.outerHTML } | Should Be "<body></body>"
        }

        It "Returns an array with multiple elements" {
            $Body = ($Node.GetElementsByTagName("body"))[0]
            $Body.innerHTML = @"
<ul>
<li>a</li>
<li>b</li>
</ul>
<ul>
<li>c</li>
<li>d</li>
<li>e</li>
</ul>
"@
            $Lis = $Node.GetElementsByTagName("li")
            $Lis.Length | Should Be 5
            $Lis[3].innerHTML | Should Be "d"
        }
    }
}
