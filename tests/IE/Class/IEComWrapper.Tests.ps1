﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\Import-TargetPackage.ps1")

Describe "IEComWrapper" {
    BeforeEach {
        $IE = New-IEComWrapper
        $IE.SetVisible($True)
        $IE.Navigate("about:blank")
        $IE.Wait()
    }

    AfterEach {
        If ($Null -Eq $IE) {
            $IE = Get-IEComWrapper -LocationName "about:blank"
        }
        If ($IE) {
            $IE.Quit()
        }
    }

    # Context "SaveHtmlFile" {
    #     It "Can save to html file" {
    #         $testPath = "TestDrive:\test.txt"

    #         $IE.SaveHtmlFile($testPath, "UTF8")
    #         Get-Content -Path $testPath | Should Be "<html><head></head><body></body></html>"
    #     }
    # }

    Context "SaveScreenShot" {
        It "Can create a file" {
            $testPath = Join-Path $TestDrive 'test.png'

            $IE.SaveScreenShot($testPath)
            Test-Path -Path $testPath | Should Be $True
        }

        It "Throws when domain path is passed" {
            $testPath = "TestDrive:\test2.png"

            { $IE.SaveScreenShot($testPath) } | Should Throw
        }
    }
}

Describe "IE" {
    Context "Senario Tests" {
        It "Can open google result page" {
            # "(1)Googleの検索結果一覧を取得してみる"
            $IE = New-IEComWrapper
            $IE.SetVisible($True)
            $IE.Navigate("https://www.google.co.jp/search?q=test")
            $IE.Wait()
            $IE.WaitForElement("input.noHIxc")
            $Doc = $IE.GetDocumentElementWrapper()
            $searchBox = $Doc.QuerySelector("input.noHIxc")
            $searchBox.Node.value | Should Be "test"
            $IE.Quit()
        }

        It "Can run google search" {
            # "(2)別のキーワードで検索してみる"
            $IE = New-IEComWrapper
            $IE.SetVisible($True)
            $IE.Navigate("https://www.google.co.jp/search?q=test")
            $IE.Wait()
            $IE.WaitForElement("input.noHIxc")
            $Doc = $IE.GetDocumentElementWrapper()
            $searchBox = $Doc.QuerySelector("input.noHIxc")
            $searchBox.Node.Value = "time japan"
            $Doc = $IE.GetDocumentElementWrapper()
            $btn = $Doc.GetElementById("qdClwb")
            $btn.Node.click()
            $IE.Wait()
            $Doc = $IE.GetDocumentElementWrapper()
            $resultElm = $Doc.QuerySelector("title")
            $resultElm.Node.innerText | Should BeLike "time japan - Google*"
            $IE.Quit()
        }

        It "Can handle dom" {
            #dom操作を試みる
            $IE = New-IEComWrapper
            $IE.SetVisible($True)
            $IE.Navigate("about:blank")
            $Doc = $IE.GetDocumentWrapper()
            $body = ($Doc.GetElementsByTagName("body"))[0]

            #HTMLを直接書き込む
            $body.Node.innerHTML = @"
<div>
<input type="radio" name="rd" value="1">a
<input type="radio" name="rd" value="2">b
<input type="radio" name="rd" value="3">c
</div>
"@

            #Dom操作でラジオボタンを追加
            $IE = Get-IEComWrapper -LocationName "about:blank"
            $Doc = $IE.GetDocumentWrapper()
            $radio_div = $Doc.GetElementsByName("rd")[0].GetParentNode()
            $radio_d = $Doc.InvokeComMethodAndGetIEComNodeWrapper("createElement", "input")[0]
            ### Elementは createElementを持たない。Documentのメソッドのため。
            # $radio_d = $Doc.InvokeComMethodAndGetIEComElementWrapper("createElement", "input")[0]
            # $radio_d = $IE.Document.createElement("input")
            $radio_d.InvokeComMethod("setAttribute", @("type", "radio")) | Out-Null
            $radio_d.InvokeComMethod("setAttribute", @("name", "rd")) | Out-Null

            $radio_div.InvokeComMethod("appendChild", $radio_d.Node) | Out-Null
            $radio_txt_d = $Doc.InvokeComMethodAndGetIEComNodeWrapper("createTextNode", "d")
            ### Elementは createTextNodeを持たない。Documentのメソッドのため。
            # $radio_txt_d = $Doc.InvokeComMethodAndGetIEComElementWrapper("createTextNode", "d")
            # $radio_d = $IE.Document.createTextNode("d")
            $radio_div.InvokeComMethod("appendChild", $radio_txt_d.Node) | Out-Null

            #Dom操作でラジオボタンをチェック
            $radios = $Doc.GetElementsByName("rd")
            $radio = $radios[1]
            $radio.InvokeComMethod("setAttribute", "checked") | Out-Null
            Start-Sleep 4

            $SelectedElm = $Doc.QuerySelector("input[name='rd'][checked='']")
            $SelectedElm.Node.value | Should Be "2"
            $IE.Quit()
        }
    }
}
